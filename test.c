    #include <stdio.h>
    #include <stdlib.h>
    #include <omp.h>
    #include <time.h>
    #include <unistd.h>
    #include "functions.c"

    unsigned long long power(unsigned long long number, int exponent){
        unsigned long long result = 1;
        for(int i = 0; i < exponent; ++i){
            result *= number;
        }
        return result;
    }
    int main(){
        int size = 4;
        int watki = 8;
        // int* test_tab = (int*)malloc(sizeof(int) * size);
        int test_tab[10];
        unsigned long long wyznacznik = power(2, size) - 1;
        printf("%llu\n", wyznacznik);
        double tab_czas = omp_get_wtime();
        // #pragma omp parallel num_threads(8)
        // {
        // while(test_tab[0] != -1){
        //     find_next_combination(test_tab, size);
        //     // for(int i = 0; i < size; ++i){
        //     //     printf("%d", test_tab[i]);
        //     // }
        //     // printf("\n");
        // }
        // }
        unsigned long long L = 0;
        int j;
        #pragma omp parallel for num_threads(watki) reduction (+:L) private(test_tab, j)
        for(unsigned long long i = 0; i < wyznacznik; ++i){
            ++L;
            for(j = size; j > 0; --j){
                test_tab[j] = (L >> (j - 1)) % 2;
                // printf("%d", test_tab[j]);
                printf("%d\n", j);
            }
            printf("\n");
        }
        tab_czas = omp_get_wtime() - tab_czas;
        // free(test_tab);
        printf("%llu: %llu\n", wyznacznik, L);
        printf("Generating all combinations for n=%d took %lfs\n", size, tab_czas);
        clock_t czas = clock();
        sleep(1);
        czas = clock() - czas;
        printf("Time taken: %lfs\n", (double) czas / CLOCKS_PER_SEC);
        #pragma omp parallel num_threads(watki)
        { 
            #ifdef _OPENMP
            sleep(1);
            #endif 
        }
        czas = clock() - czas;
        printf("Time taken by OpenMP: %lfs\n", (double) czas / CLOCKS_PER_SEC);
        #ifdef _OPENMP
        printf("Per process: %lfs\n", (double) czas / CLOCKS_PER_SEC / watki);
        #endif
        // struct Graph* graf_testowy = generate_graphs_from_file("exampledata.txt");
        struct Graph* graf_testowy = generate_random_graphs(1, 10, 50);
        struct Graph summed_graph = sum_graphs(graf_testowy, 1);
        printf("Zsumowany graf ma %d wierzcholkow\n", summed_graph.vertex_count);
        double czas_szukania = omp_get_wtime();
        int* solution;
        int vertex_count = 0;
        int* best_solution = (int*)malloc(sizeof(int) * 0), best_count = INF, best_thread = INF;
        #pragma omp parallel for num_threads(watki) private(solution, vertex_count) shared(best_solution, best_count, best_thread)
        for(int i = 0; i < watki; ++i){
            // printf("Watek %d, probuje znalezc dla grafu: %d, maks watek to %d\n", i, summed_graph.vertex_count, watki - 1);
            solution = find_min_cover_parallel(summed_graph, i, watki - 1);
            // printf("Watek %d, znalazlem\n", i);
            printf("%d: ", i);
            vertex_count = -1;
            for(int j = 0; j < summed_graph.vertex_count; ++j){
                printf("%d ", solution[j]);
                if(solution[j] == -1){
                    solution = (int*)realloc(solution, sizeof(int) * (j));
                    vertex_count = j;
                    break;
                }
            }
            if(vertex_count == -1){
                vertex_count = INF;
            }
            printf("\n");
            #pragma omp critical
            {
                if(best_count > vertex_count){
                    best_thread = i;
                    best_count = vertex_count;
                    best_solution = realloc(best_solution, sizeof(int) * best_count);
                    for(int j = 0; j < best_count; ++j){
                        best_solution[j] = solution[j];
                    }
                }
            }
        }
        czas_szukania = omp_get_wtime() - czas_szukania;
        printf("Parallelly found solution(by thread %d):\n", best_thread);
        for(int i = 0; i < best_count; ++i){
            if(best_solution[i] == -1){
                break;
            }
            printf("%d ", best_solution[i]);
        }
        printf("\nTime taken: %lfs\n", czas_szukania);
        czas_szukania = omp_get_wtime();
        solution = find_min_cover_full_search(summed_graph);
        czas_szukania = omp_get_wtime() - czas_szukania;
        printf("Normally found solution:\n");
        for(int i = 0; i < summed_graph.vertex_count; ++i){
            if(solution[i] == -1){
                break;
            }
            printf("%d ", solution[i]);
        }
        printf("\nTime taken: %lfs\n", czas_szukania);
        free(solution);
        free(graf_testowy);
        free(best_solution);
        return EXIT_SUCCESS;
    }


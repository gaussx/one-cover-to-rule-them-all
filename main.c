#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include "functions.c"
#define START_TEMP 1
#define MIN_TEMP 0.001
#define COOLING_RATE 0.9
#define MAX_NEIGHBOURS 100


int main(){
    char choice = 'y', menu_choice, parallel_choice;
    char filename[1024];
    struct Graph base_graph, summed_graph;
    base_graph.vertex_count = 0;
    base_graph.matrix = NULL;
    summed_graph.vertex_count = 0;
    summed_graph.matrix = NULL;
    struct Graph* edge_list = NULL;
    int vertices = -1, remove_choice, cover_vertex_count, CURRENT_SETS = 0, probability;
    int* solution = NULL, *min_solution = NULL, *heur_solution = NULL, *annealing_solution = NULL;
    float min_cover_vertex_count, heur_cover_vertex_count, annealing_cover_vertex_count;
    double full_search_time, heur_search_time, annealing_search_time;
    do{
        printf("Menu:\n1. Initialize graph\n2. Add set of edges to the list manually\n3. Read sets of edges from a file\n4. Generate graphs randomly\n5. Remove set of edges from the list\n6. Print stored sets of edges\n7. Find vertex cover with a heuristic method\n8. Find minimal vertex cover with brute force\n9. Find vertex cover with all methods and compare performance.\na. Compare performance of chosen algorithms using randomly generated data sets\nb. Find the cover using simulated annealing\nc. Compare sequential and parallel implementations' execution time\nChoice: ");
        scanf(" %c", &menu_choice);
        switch(menu_choice){
            case '1':
                if(CURRENT_SETS > 0){
                    for(int i = 0; i < CURRENT_SETS; ++i){
                        for(int j = 0; j < edge_list[0].vertex_count; ++j){
                            free(edge_list[i].matrix[j]);
                        }
                        //free(edge_list[i].matrix);
                    }
                    free(edge_list);
                    edge_list = NULL;
                }
                for(int i = 0; i < base_graph.vertex_count; ++i){
                    free(base_graph.matrix[i]);
                }
                free(base_graph.matrix);
                CURRENT_SETS = 0;
                vertices = -1;
                printf("Enter number of vertices: ");
                scanf("%d", &vertices);
                if(vertices <= 0){
                    printf("A graph must have more than 0 vertices, enter a correct number: ");
                    scanf("%d", &vertices);
                }
                base_graph = initialize_graph(vertices);
                printf("Graph initialized.\n");
                break;
            case '2':
                if(vertices == -1){
                    printf("No graph was given.\n");
                    break;
                }
                edge_list = (struct Graph*)realloc(edge_list, (CURRENT_SETS + 1) * sizeof(struct Graph));
                edge_list[CURRENT_SETS] = initialize_graph(vertices);
                edge_list[CURRENT_SETS] = generate_graph_from_input(vertices);
                ++CURRENT_SETS;
                printf("Set of edges added.\n");
                break;
            case '3':
                printf("Enter the filename: ");
                scanf("%s", filename);
                if(edge_list != NULL){
                    for(int i = 0; i < CURRENT_SETS; ++i){
                        for(int j = 0; j < edge_list[0].vertex_count; ++j)
                            free(edge_list[i].matrix[j]);
                        free(edge_list[i].matrix);
                    }
                    free(edge_list);
                }
                edge_list = generate_graphs_from_file(filename);
                CURRENT_SETS = SETS_IN_FILE;
                if(CURRENT_SETS > 0){
                    vertices = edge_list[0].vertex_count;
                }
                if(base_graph.matrix != NULL){
                    for(int i = 0; i < base_graph.vertex_count; ++i){
                        free(base_graph.matrix[i]);
                    }
                    free(base_graph.matrix);
                }
                base_graph = initialize_graph(vertices);
                printf("File read successfully.\n");
                break;
            case '4':
                printf("Enter number of graphs to be generated: ");
                scanf("%d", &CURRENT_SETS);
                printf("Enter number of vertices of each graph: ");
                scanf("%d", &vertices);
                printf("Enter the percentage probability for creating an edge(eg. 50%%): ");
                scanf("%d", &probability);
                if(edge_list != NULL){
                    for(int i = 0; i < CURRENT_SETS; ++i){
                        for(int j = 0; j < edge_list[0].vertex_count; ++j){
                            free(edge_list[i].matrix[j]);
                        }
                        free(edge_list[i].matrix);
                    }
                    free(edge_list);
                }
                edge_list = generate_random_graphs(CURRENT_SETS, vertices, probability);
                printf("Graphs generated successfully.\n");
                break;
                break;
            case '5':
                if(CURRENT_SETS == 0){
                    printf("No sets were given.\n");
                    break;
                }
                printf("Enter the number of the edge set to be removed: ");
                scanf("%d", &remove_choice);
                --remove_choice;
                while(remove_choice < 0 || remove_choice >= CURRENT_SETS){
                    printf("Incorrect choice(1-%d): ", CURRENT_SETS);
                    scanf("%d", &remove_choice);
                    --remove_choice;
                }
                if(remove_choice == CURRENT_SETS - 1){
                    for(int j = 0; j < edge_list[0].vertex_count; ++j){
                        free(edge_list[remove_choice].matrix[j]);
                    }
                    free(edge_list[remove_choice].matrix);
                    edge_list = (struct Graph*)realloc(edge_list, (CURRENT_SETS - 1) * sizeof(struct Graph));
                    --CURRENT_SETS;
                    printf("Set removed.\n");
                }
                else{
                    edge_list[remove_choice].vertex_count = edge_list[CURRENT_SETS - 1].vertex_count;
                    for(int i = 0; i < edge_list[0].vertex_count; ++i){
                        for(int j = 0; j < edge_list[0].vertex_count; ++j){
                            edge_list[remove_choice].matrix[i][j] = edge_list[CURRENT_SETS - 1].matrix[i][j];
                        }
                    }
                    for(int j = 0; j < edge_list[0].vertex_count; ++j){
                        free(edge_list[CURRENT_SETS - 1].matrix[j]);
                    }
                    free(edge_list[CURRENT_SETS - 1].matrix);
                    edge_list = (struct Graph*)realloc(edge_list, (CURRENT_SETS - 1) * sizeof(struct Graph));
                    --CURRENT_SETS;
                    printf("Set removed. Set %d(formerly) was moved to chosen position.\n", CURRENT_SETS + 1);
                }
                if(CURRENT_SETS == 0){
                    free(edge_list);
                    edge_list = NULL;
                }
                break;
            case '6':
                if(CURRENT_SETS == 0){
                    printf("No sets were given.\n");
                    break;
                }
                for(int i = 0; i < CURRENT_SETS; ++i){
                    printf("\n\n--------------------\n\n");
                    printf("%d:\n", i + 1);
                    print_graph(edge_list[i]);
                }
                break;
            case '7':{
                cover_vertex_count = 0;
                summed_graph = sum_graphs(edge_list, CURRENT_SETS);
                //printf("Summed graph:\n\n");
                //print_graph(summed_graph);
                solution = find_cover_heuristic(summed_graph);
                for(int i = 0; i < summed_graph.vertex_count; ++i){
                    if(solution[i] == -1){
                        solution = (int*)realloc(solution, sizeof(int) * (i));
                        cover_vertex_count = i;
                        break;
                    }
                }
                printf("Vertex cover for given edge sets consists of: ");
                for(int i = 0; i < cover_vertex_count; ++i){
                    printf("%d", solution[i]);
                    if(i + 1 != cover_vertex_count) printf(", ");
                    else printf(".\n");
                }
                for(int i = 0; i < summed_graph.vertex_count; ++i){
                    free(summed_graph.matrix[i]);
                }
                free(summed_graph.matrix);
                summed_graph.matrix = NULL;
                free(solution);
                solution = NULL;
                break;
            }
            case '8':
                if(CURRENT_SETS == 0){
                    printf("No sets were given.\n");
                    break;
                }
                summed_graph = sum_graphs(edge_list, CURRENT_SETS);
                clock_t timer = omp_get_wtime();
                solution = find_min_cover_full_search(summed_graph);
                for(int i = 0; i < edge_list[0].vertex_count; ++i){
                    if(solution[i] == -1){
                        solution = (int*)realloc(solution, sizeof(int) * (i));
                        cover_vertex_count = i;
                        break;
                    }
                }
                timer = omp_get_wtime() - timer;
                printf("Solution found in %lfs\n", (double) timer / CLOCKS_PER_SEC);
                printf("Minimal vertex cover for given edge sets consists of: ");
                for(int i = 0; i < cover_vertex_count; ++i){
                    printf("%d", solution[i]);
                    if(i + 1 != cover_vertex_count) printf(", ");
                    else printf(".\n");
                }
                for(int i = 0; i < summed_graph.vertex_count; ++i){
                    free(summed_graph.matrix[i]);
                }
                free(summed_graph.matrix);
                summed_graph.matrix = NULL;
                free(solution);
                break;
            case '9':{
                if(CURRENT_SETS == 0){
                    printf("No sets were given.\n");
                    break;
                }
                clock_t czas = clock();
                min_solution = find_min_cover_full_search(sum_graphs(edge_list, CURRENT_SETS));
                for(int i = 0; i < edge_list[0].vertex_count; ++i){
                    if(min_solution[i] == -1){
                        min_solution = (int*)realloc(min_solution, sizeof(int) * (i));
                        min_cover_vertex_count = i;
                        break;
                    }
                }
                czas = clock() - czas;
                full_search_time = (double) czas / CLOCKS_PER_SEC;
                printf("Minimal vertex cover for given edge sets consists of the following vertices: ");
                for(int i = 0; i < min_cover_vertex_count; ++i){
                    printf("%d", min_solution[i]);
                    if(i + 1 != min_cover_vertex_count) printf(", ");
                    else printf(".\n");
                }
                czas = clock();
                heur_solution = find_cover_heuristic(sum_graphs(edge_list, CURRENT_SETS));
                for(int i = 0; i < edge_list[0].vertex_count; ++i){
                    if(heur_solution[i] == -1){
                        heur_solution = (int*)realloc(heur_solution, sizeof(int) * (i));
                        heur_cover_vertex_count = i;
                        break;
                    }
                }
                czas = clock() - czas;
                heur_search_time = (double) czas / CLOCKS_PER_SEC;
                printf("Vertex cover found by the heuristic for given edge sets consists of the following vertices: ");
                for(int i = 0; i < heur_cover_vertex_count; ++i){
                    printf("%d", heur_solution[i]);
                    if(i + 1 != heur_cover_vertex_count) printf(", ");
                    else printf(".\n");
                }
                printf("The heuristic solution is off from the minimal solution by %.0f%%", (heur_cover_vertex_count/min_cover_vertex_count) - 1);
                if(heur_cover_vertex_count/min_cover_vertex_count - 1 == 0){
                    printf(" (it has found the optimal solution)");
                }
                czas = clock();
                annealing_solution = find_cover_annealing(sum_graphs(edge_list, CURRENT_SETS), START_TEMP, MIN_TEMP, COOLING_RATE, MAX_NEIGHBOURS);
                for(int i = 0; i < edge_list[0].vertex_count; ++i){
                    if(annealing_solution[i] == -1){
                        annealing_solution = (int*)realloc(annealing_solution, sizeof(int) * (i));
                        annealing_cover_vertex_count = i;
                        break;
                    }
                }
                czas = clock() - czas;
                annealing_search_time = (double) czas / CLOCKS_PER_SEC;
                printf("Vertex cover found by simulated annealing for given edge sets consists of the following vertices: ");
                for(int i = 0; i < annealing_cover_vertex_count; ++i){
                    printf("%d", annealing_solution[i]);
                    if(i + 1 != annealing_cover_vertex_count) printf(", ");
                    else printf(".\n");
                }
                printf("Simulated annealing's solution is off from the minimal solution by %.0f%%", (annealing_cover_vertex_count/min_cover_vertex_count) - 1);
                if(annealing_cover_vertex_count/min_cover_vertex_count - 1 == 0){
                    printf(" (it has found the optimal solution)");
                }
                printf(".\nSimulated annealing has finished in %lfs, whereas the brute force took %lfs.\n", annealing_search_time, full_search_time);
                free(min_solution);
                free(heur_solution);
                free(annealing_solution);
                min_solution = NULL;
                heur_solution = NULL;
                annealing_solution = NULL;
                break;
            }
            case 'a':{
                srand(time(NULL));
                int data_sets, testing_graphs, testing_vertices, testing_probability;
                int heur_flag = 0, brute_flag = 0, annealing_flag = 0;
                int heur_found_optimal = 0, heur_found_suboptimal = 0, annealing_found_optimal = 0, annealing_found_suboptimal = 0;
                struct Graph *testing_edge_list = NULL;
                char user_choice[10];
                for(int i = 0; i < 10; ++i){
                    user_choice[i] = 'n';
                }
                printf("Enter the ID's of algorithms you want to test(eg. 123 for all of them).\nIDs:\n1. Heuristic algorithm\n2. Brute force\n3. Simulated annealing\nChoice: ");
                scanf("%s", user_choice);
                for(int i = 0; i < 3; ++i){
                    switch (user_choice[i]) {
                        case '1':
                            heur_flag = 1;
                            break;
                        case '2':
                            brute_flag = 1;
                            break;
                        case '3':
                            annealing_flag = 1;
                            break;
                    }
                }

                printf("Enter number of data sets you want to calculate for: ");
                scanf("%d", &data_sets);
                printf("Enter number of edge sets for each data set: ");
                scanf("%d", &testing_graphs);
                printf("Enter number of vertices for each data set: ");
                scanf("%d", &testing_vertices);
                printf("Enter the percentage probability for creating an edge in a graph(eg. '50' if you want 50%%): ");
                scanf("%d", &testing_probability);
                double heur_times[data_sets], brute_times[data_sets], annealing_times[data_sets], heur_optimum_distance[data_sets], annealing_optimum_distance[data_sets];
                clock_t czas;
                for(int i = 0; i < data_sets; ++i){
                    testing_edge_list = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
                    summed_graph = sum_graphs(testing_edge_list, testing_graphs);
                    if(heur_flag){
                        czas = clock();
                        heur_solution = find_cover_heuristic(summed_graph);
                        for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
                            if(heur_solution[j] == -1){
                                heur_solution = (int*)realloc(heur_solution, sizeof(int) * (j));
                                heur_cover_vertex_count = j;
                                break;
                            }
                        }
                        czas = clock() - czas;
                        heur_search_time = (double) czas / CLOCKS_PER_SEC;
                        heur_times[i] = heur_search_time;
                    }
                    if(annealing_flag){
                        czas = clock();
                        annealing_solution = find_cover_annealing(summed_graph, START_TEMP, MIN_TEMP, COOLING_RATE, MAX_NEIGHBOURS);
                        for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
                            if(annealing_solution[j] == -1){
                                annealing_solution = (int*)realloc(annealing_solution, sizeof(int) * (j));
                                annealing_cover_vertex_count = j;
                                break;
                            }
                        }
                        czas = clock() - czas;
                        annealing_times[i] = (double) czas / CLOCKS_PER_SEC;
                    }
                    if(brute_flag){
                        czas = clock();
                        min_solution = find_min_cover_full_search(summed_graph);
                        for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
                            if(min_solution[j] == -1){
                                min_solution = (int*)realloc(min_solution, sizeof(int) * (j));
                                min_cover_vertex_count = j;
                                break;
                            }
                        }
                        czas = clock() - czas;
                        full_search_time = (double) czas / CLOCKS_PER_SEC;
                        brute_times[i] = full_search_time;
                        if(heur_flag){
                            heur_optimum_distance[i] = (heur_cover_vertex_count / min_cover_vertex_count) - 1;
                            if(heur_optimum_distance[i] > 0){
                                ++heur_found_suboptimal;
                            }
                            else{
                                ++heur_found_optimal;
                            }
                        }
                        if(annealing_flag){
                            annealing_optimum_distance[i] = (annealing_cover_vertex_count / min_cover_vertex_count) - 1;
                            if(annealing_optimum_distance[i] > 0){
                                ++annealing_found_suboptimal;
                            }
                            else{
                                ++annealing_found_optimal;
                            }
                        }
                    }
                    free(heur_solution);
                    free(min_solution);
                    free(annealing_solution);
                    heur_solution = NULL;
                    min_solution = NULL;
                    annealing_solution = NULL;
                    for(int i = 0; i < testing_vertices; ++i){
                        free(summed_graph.matrix[i]);
                    }
                    free(summed_graph.matrix);
                    summed_graph.matrix = NULL;
                    for(int i = 0; i < testing_graphs; ++i){
                        for(int j = 0; j < testing_vertices; ++j){
                            free(testing_edge_list[i].matrix[j]);
                        }
                        free(testing_edge_list[i].matrix);
                    }
                    free(testing_edge_list);
                    testing_edge_list = NULL;
                }
                double max_heur_opt_distance = 0, max_annealing_opt_distance = 0;
                // printf("Times achieved for the heuristic:\n");
                double avg_heur_time = 0, avg_brute_time = 0, avg_annealing_time = 0, avg_heur_optimum_distance = 0, avg_annealing_optimum_distance = 0;
                if(brute_flag){
                    // printf("Times achieved for the brute force algorithm:\n");
                    for(int i = 0; i < data_sets; ++i){
                        // printf("Dataset %d: %lfs\n", i+1, brute_times[i]);
                        avg_brute_time += brute_times[i];
                    }
                    avg_brute_time /= data_sets;
                    printf("     ╔═════════════════════════════╗\n");
                    printf("     ║         BRUTE FORCE         ║\n");
                    printf("     ╚═════════════════════════════╝\n\n");
                    printf("  * Average time for the brute force algorithm: %lfs\n\n", avg_brute_time);
                }
                if(heur_flag){
                    for(int i = 0; i < data_sets; ++i){
                        // printf("Dataset %d: %lfs\n", i+1, heur_times[i]);
                        avg_heur_time += heur_times[i];
                        avg_heur_optimum_distance += heur_optimum_distance[i];
                        if (heur_optimum_distance[i] > max_heur_opt_distance){
                            max_heur_opt_distance = heur_optimum_distance[i];
                        }
                    }
                    avg_heur_time /= data_sets;
                    avg_heur_optimum_distance /= data_sets;
                    printf("     ╔═════════════════════════════╗\n");
                    printf("     ║       GREEDY ALGORITHM      ║\n");
                    printf("     ╚═════════════════════════════╝\n\n");
                    printf("  * Average execution time: %lfs\n", avg_heur_time);
                    if(brute_flag){
                        printf("  * How many times an optimal solution was found: %d\n", heur_found_optimal);
                        printf("  * How many times an optimal solution was missed: %d\n", heur_found_suboptimal);
                        printf("  * Average percentage error between found solution and the optimal solution: %.6lf%%\n", 100*avg_heur_optimum_distance);
                        printf("  * The largest percentage error between found solution and the optimal solution: %lf\n", 100*max_heur_opt_distance);
                    }
                    printf("\n");
                }
                if(annealing_flag){
                    for(int i = 0; i < data_sets; ++i){
                        // printf("Dataset %d: %lfs\n", i+1, annealing_times[i]);
                        avg_annealing_time += annealing_times[i];
                        avg_annealing_optimum_distance += annealing_optimum_distance[i];
                        if (annealing_optimum_distance[i] > max_annealing_opt_distance){
                            max_annealing_opt_distance = annealing_optimum_distance[i];
                        }
                    }
                    avg_annealing_time /= data_sets;
                    avg_annealing_optimum_distance /= data_sets;
                    printf("     ╔═════════════════════════════╗\n");
                    printf("     ║     SIMULATED ANNEALING     ║\n");
                    printf("     ╚═════════════════════════════╝\n\n");
                    printf("  * Average time: %lfs\n", avg_annealing_time);
                    if(brute_flag){
                        printf("  * How many times an optimal solution was found: %d\n", annealing_found_optimal);
                        printf("  * How many times an optimal solution was missed: %d\n", annealing_found_suboptimal);
                        printf("  * Average percentage error between found solution and the optimal solution: %.6lf%%\n", 100*avg_annealing_optimum_distance);
                        printf("  * The largest percentage error between found solution and the optimal solution: %lf\n", 100*max_annealing_opt_distance);
                    }
                    printf("\n");
                }
                break;
            }
            case 'b':
                annealing_cover_vertex_count = 0;
                summed_graph = sum_graphs(edge_list, CURRENT_SETS);
                //printf("Summed graph:\n\n");
                //print_graph(summed_graph);
                annealing_solution = find_cover_annealing(summed_graph, START_TEMP, MIN_TEMP, COOLING_RATE, MAX_NEIGHBOURS);
                for(int i = 0; i < summed_graph.vertex_count; ++i){
                    if(annealing_solution[i] == -1){
                        annealing_solution = (int*)realloc(annealing_solution, sizeof(int) * (i));
                        annealing_cover_vertex_count = i;
                        break;
                    }
                }
                printf("Vertex cover for given edge sets consists of: ");
                for(int i = 0; i < annealing_cover_vertex_count; ++i){
                    printf("%d", annealing_solution[i]);
                    if(i + 1 != annealing_cover_vertex_count) printf(", ");
                    else printf(".\n");
                }
                for(int i = 0; i < summed_graph.vertex_count; ++i){
                    free(summed_graph.matrix[i]);
                }
                free(summed_graph.matrix);
                summed_graph.matrix = NULL;
                free(annealing_solution);
                annealing_solution = NULL;
                break;
            case 'c':
                printf("Choose what to test the execution time of:\n1. Greedy algorithm: solving N datasets\n2. Brute force: finding the solution\n3. Simulated annealing: 1 vs N attempts\nChoice: ");
                scanf(" %c", &parallel_choice);
                switch (parallel_choice) {
                    case '1':{
                        int data_sets, testing_graphs, testing_vertices, testing_probability, testing_threads;
                        printf("Greedy algorithm showcase!\n");
                        printf("Enter number of data sets you want to calculate for: ");
                        scanf("%d", &data_sets);
                        printf("Enter number of edge sets for each data set: ");
                        scanf("%d", &testing_graphs);
                        printf("Enter number of vertices for each data set: ");
                        scanf("%d", &testing_vertices);
                        printf("Enter the percentage probability for creating an edge in a graph(eg. '50' if you want 50%%): ");
                        scanf("%d", &testing_probability);
                        printf("Enter number of threads used for parallel processing: ");
                        scanf("%d", &testing_threads);
                        struct Graph* testing_edge_list = generate_random_graphs(data_sets, testing_vertices, testing_probability);
                        // struct Graph **testing_edge_list = NULL;
                        // testing_edge_list = (struct Graph**)malloc(sizeof(struct Graph*) * data_sets);
                        // for(int i = 0; i < data_sets; ++i){
                        //     testing_edge_list[0] = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
                        // }
                        double seq_time, par_time;
                        double czas = omp_get_wtime();
                        for(int i = 0; i < data_sets; ++i){
                            // testing_edge_list = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
                            // summed_graph = sum_graphs(testing_edge_list, testing_graphs);
                            heur_solution = find_cover_heuristic(testing_edge_list[i]);
                            // for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
                            //     if(heur_solution[j] == -1){
                            //         heur_solution = (int*)realloc(heur_solution, sizeof(int) * (j));
                            //         heur_cover_vertex_count = j;
                            //         break;
                            //     }
                            // }
                        }
                        czas = omp_get_wtime() - czas;
                        seq_time = czas;
                        printf("Solving %d datasets sequentially took %lf seconds.\n", data_sets, seq_time);
                        czas = omp_get_wtime();
                        #pragma omp parallel for num_threads(testing_threads) //private(testing_edge_list, heur_solution)
                        for(int i = 0; i < data_sets; ++i){
                            // printf("%d\n", i);
                            // testing_edge_list = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
                            // summed_graph = sum_graphs(testing_edge_list, testing_graphs);
                            heur_solution = find_cover_heuristic(testing_edge_list[i]);
                            // for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
                            //     if(heur_solution[j] == -1){
                            //         heur_solution = (int*)realloc(heur_solution, sizeof(int) * (j));
                            //         heur_cover_vertex_count = j;
                            //         break;
                            //     }
                            // }
                        }
                        czas = omp_get_wtime() - czas;
                        par_time = czas;
                        printf("Solving %d datasets in parallel took %lf seconds.\n", data_sets, par_time);
                        break;
                    }
                    case '2':
                        printf("Brute force search showcase!\n");
                        //pass
                        break;
                    case '3':{
                        int attempts, testing_graphs, testing_vertices, testing_probability;
                        printf("Simulated annealing showcase!\n");
                        printf("Enter number of edge sets: ");
                        scanf("%d", &testing_graphs);
                        printf("Enter number of vertices: ");
                        scanf("%d", &testing_vertices);
                        printf("Enter the percentage probability for creating an edge between two vertices(eg. '50' if you want 50%%): ");
                        scanf("%d", &testing_probability);
                        printf("Enter number of threads: ");
                        scanf( "%d", &attempts);
                        struct Graph* local_edge_list = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
                        annealing_cover_vertex_count = 0;
                        summed_graph = sum_graphs(local_edge_list, testing_graphs);
                        //printf("Summed graph:\n\n");
                        //print_graph(summed_graph);
                        clock_t czas = omp_get_wtime();
                        int* annealing_solution = find_cover_annealing(summed_graph, START_TEMP, MIN_TEMP, COOLING_RATE, MAX_NEIGHBOURS);
                        for(int i = 0; i < summed_graph.vertex_count; ++i){
                            if(annealing_solution[i] == -1){
                                annealing_solution = (int*)realloc(annealing_solution, sizeof(int) * (i));
                                annealing_cover_vertex_count = i;
                                break;
                            }
                        }
                        czas = omp_get_wtime() - czas;
                        printf("Took 1 attempt in %lfs.\n", (double) czas);
                        printf("Least vertex cover found within 1 attempt for given edge sets consists of(%f): ", annealing_cover_vertex_count);
                        for(int i = 0; i < annealing_cover_vertex_count; ++i){
                            printf("%d", annealing_solution[i]);
                            if(i + 1 != annealing_cover_vertex_count) printf(", ");
                            else printf(".\n");
                        }

                        printf("\n");
                        int best = INF;
                        int* best_solution = (int*)malloc(sizeof(int) * 0);
                        czas = omp_get_wtime();
                        #pragma omp parallel for private(annealing_solution, annealing_cover_vertex_count) num_threads(attempts) shared(best, best_solution)
                        for(int i = 0; i < attempts; ++i){
                            annealing_solution = find_cover_annealing(summed_graph, START_TEMP, MIN_TEMP, COOLING_RATE, MAX_NEIGHBOURS);
                            for(int i = 0; i < summed_graph.vertex_count; ++i){
                                if(annealing_solution[i] == -1){
                                    annealing_solution = (int*)realloc(annealing_solution, sizeof(int) * (i));
                                    annealing_cover_vertex_count = i;
                                    break;
                                }
                            }
                            printf("%d - found solution of %f vertices\n", i, annealing_cover_vertex_count);
                            #pragma omp critical
                            {
                                if(best > annealing_cover_vertex_count){
                                    best = (int)annealing_cover_vertex_count;
                                    best_solution = realloc(best_solution, sizeof(int) * best);
                                    for(int i = 0; i < best; ++i){
                                        best_solution[i] = annealing_solution[i];
                                    }
                                }
                            }
                        }
                        czas = omp_get_wtime() - czas;
                        printf("Took %d attempts in %lfs.\n", attempts, (double)czas);
                        printf("Least vertex cover found within %d attempts for given edge sets consists of(%d): ", attempts, best);
                        for(int i = 0; i < best; ++i){
                            printf("%d", best_solution[i]);
                            if(i + 1 != best) printf(", ");
                            else printf(".\n");
                        }
                        for(int i = 0; i < summed_graph.vertex_count; ++i){
                            free(summed_graph.matrix[i]);
                        }
                        free(summed_graph.matrix);
                        summed_graph.matrix = NULL;
                        free(annealing_solution);
                        annealing_solution = NULL;
                        free(best_solution);

                        break;
                    }
                    default:
                        printf("Incorrect choice!\n");
                        break;
                }
                break;
            default:
                printf("Incorrect choice!\n");
                break;
        }
        while ((getchar()) != '\n'); //clear buffer
        printf("Continue?[y/n] ");
        scanf(" %c", &choice);
    }while(choice == 'y' || choice == 'Y');


    if(base_graph.matrix != NULL){
        for(int i = 0; i < base_graph.vertex_count; ++i){
            free(base_graph.matrix[i]);
        }
        free(base_graph.matrix);
    }
    if(summed_graph.matrix != NULL){
        for(int i = 0; i < summed_graph.vertex_count; ++i){
            free(summed_graph.matrix[i]);
        }
        free(summed_graph.matrix);
    }
    //printf("Po ostatnim\n");
    if(edge_list != NULL){
        for(int i = 0; i < CURRENT_SETS; ++i){
            for(int j = 0; j < edge_list[0].vertex_count; ++j)
                free(edge_list[i].matrix[j]);
            free(edge_list[i].matrix);
        }
        free(edge_list);
    }   
    if(solution != NULL)
        free(solution);
    if(heur_solution != NULL)
        free(heur_solution);
    if(min_solution != NULL)
        free(min_solution);
    if(annealing_solution != NULL)
        free(annealing_solution);
    return 0;
}
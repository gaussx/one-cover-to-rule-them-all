#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#define INF 2147483647

struct Graph{
    int vertex_count;
    int **matrix;
};

struct Graph initialize_graph(int vertices){
    struct Graph graph;
    graph.vertex_count = vertices;
    graph.matrix = (int**)malloc(graph.vertex_count * sizeof(int*));
        for(int i = 0; i < graph.vertex_count; ++i){
            graph.matrix[i] = (int*)malloc(graph.vertex_count * sizeof(int));
            for(int j = 0; j < graph.vertex_count; ++j){
                graph.matrix[i][j] = 0;
            }
        }
    return graph;
}

struct Graph generate_graph_from_input(int vertices){
    struct Graph result_graph = initialize_graph(vertices);
    int vertex1 = 0, vertex2 = 0;
    printf("Keep entering pairs of vertices like this - vertex1 vertex2 - to add edges.\nTo stop, enter either a pair with either a negative value or a value of %d or more\n(we index vertices from 0).\n", vertices);
    while(vertex1 >= 0 && vertex2 >= 0){
        scanf("%d%d", &vertex1, &vertex2);
        if(vertex1 < 0 || vertex1 > vertices - 1 || vertex2 < 0 || vertex2 > vertices - 1){
            break;
        }
        result_graph.matrix[vertex1][vertex2] = 1;
        result_graph.matrix[vertex2][vertex1] = 1;
    }
    return result_graph;
}

int SETS_IN_FILE;
struct Graph* generate_graphs_from_file(char* filename){
    FILE* file = fopen(filename, "r");
    int graphs, vertices, i = 0, space_index;
    fscanf(file, "%d%d", &graphs, &vertices);
    char line[vertices + 1];
    struct Graph* edge_list = (struct Graph*)malloc(graphs * sizeof(struct Graph));
    if(graphs <= 0){
        return edge_list;
    }
    for(int i = 0; i < graphs; ++i){
        edge_list[i] = initialize_graph(vertices);
    }
    SETS_IN_FILE = graphs;
    for(int i = 0; i < graphs; ++i){
        for(int j = 0; j < vertices; ++j){
            fscanf(file, "%s", line);
            for(int k = 0; k < vertices; ++k){
                edge_list[i].matrix[j][k] = line[k] - '0';
            }
        }
    }
    free(file);
    return edge_list;
}

struct Graph* generate_random_graphs(int graphs, int vertices, int probability){
    srand(time(NULL));
    struct Graph* edge_list = (struct Graph*)malloc(graphs * sizeof(struct Graph));
    if(graphs <= 0){
        return edge_list;
    }
    for(int i = 0; i < graphs; ++i){
        edge_list[i] = initialize_graph(vertices);
        for(int j = 0; j < vertices; ++j){
            for(int k = 0; k < vertices; ++k){
                if(k == j) continue;
                if(rand() % 100 + 1 <= probability){
                    edge_list[i].matrix[j][k] = 1;
                    edge_list[i].matrix[k][j] = 1;
                }
            }
        }
    }
    return edge_list;
}

int digit_count(int number){
    int digits = 1;
    while(((number /= 10)) > 0) ++digits;
    return digits;
}

void print_spaces(int n){
    for(int i = 0; i < n; ++i){
        printf(" ");
    }
}

void print_graph(struct Graph graph){
    int vertex_digits = digit_count(graph.vertex_count);
    print_spaces(vertex_digits + 3);
    for(int i = 0; i < graph.vertex_count; ++i){
        print_spaces(vertex_digits - digit_count(i) + 2);
        printf("%d", i);
    }
    printf("\n");
    print_spaces(vertex_digits + 3);
    for(int i = 0; i < graph.vertex_count; ++i){
        for(int j = 0; j < vertex_digits + 2; ++j)
        printf("_");
    }
    
    printf("\n");
    for(int i = 0; i < graph.vertex_count; ++i){
        print_spaces(vertex_digits - digit_count(i) + 2);
        printf("%d|", i);
        for(int j = 0; j < graph.vertex_count; ++j){
            print_spaces(vertex_digits + 1);
            printf("%d", graph.matrix[i][j]);
        }
        printf("\n");
    }

}

struct Graph sum_graphs(struct Graph* graph_list, int graph_number){
    struct Graph result_graph = initialize_graph(graph_list[0].vertex_count);
    for(int i = 0; i < result_graph.vertex_count; ++i){
        for(int j = 0; j < result_graph.vertex_count; ++j){
            for(int k = 0; k < graph_number; ++k){
                if(graph_list[k].matrix[i][j] == 1){
                    result_graph.matrix[i][j] = 1;
                    break;
                }
            }
        }
    }
    return result_graph;
}


int* find_cover_heuristic(struct Graph base_graph){
    int* result_vertices = (int*)malloc(sizeof(int) * base_graph.vertex_count);
    for(int i = 0; i < base_graph.vertex_count; ++i){
        result_vertices[i] = -1;
    }
    int solution_vertice_count = 0;
    int current_degree, max_degree, max_vertex;
    int tmp_matrix[base_graph.vertex_count][base_graph.vertex_count];
    for(int i = 0; i < base_graph.vertex_count; ++i){
        for(int j = 0; j < base_graph.vertex_count; ++j){
            tmp_matrix[i][j] = base_graph.matrix[i][j];
        }
    }
    do{
        max_degree = 0;
        for(int i = 0; i < base_graph.vertex_count; ++i){
            current_degree = 0;
            for(int j = 0; j < base_graph.vertex_count; ++j){
                if(tmp_matrix[i][j] == 1){
                    current_degree += 1;
                }
            }
            if(max_degree < current_degree){
                max_degree = current_degree;
                max_vertex = i;
            }
        }
        //printf("Max vertex: %d, its degree: %d\n", max_vertex, max_degree);
        if(max_degree == 0) break;
        result_vertices[solution_vertice_count++] = max_vertex;
        for(int i = 0; i < base_graph.vertex_count; ++i){
            tmp_matrix[max_vertex][i] = 0;
            tmp_matrix[i][max_vertex] = 0;
        }
    }while(max_degree > 0);
    return result_vertices;
}

void find_next_combination(int* combination_array, int size){
    // #pragma omp parallel for
    for(int j = size - 1; j >= -1; --j){
        if(j < 0){
            combination_array[0] = -1;
            break;
        }
        if(combination_array[j] == 0){
            combination_array[j] = 1;
            break;
        }
        else combination_array[j] = 0;
    }
    return;
}

void find_next_combination_offset(int* combination_array, int size, int offset){
    // #pragma omp parallel for
    int max = size - offset - 1;
    for(int j = max; j >= -1; --j){
        if(j < 0){
            combination_array[0] = -1;
            break;
        }
        if(combination_array[j] == 0){
            combination_array[j] = 1;
            break;
        }
        else combination_array[j] = 0;
    }
    return;
}

void find_all_combinations_offset(int size, int number, int offset){
    int* combinations = (int*)malloc(sizeof(int) * size);
    for(int i = 0; i < size; ++i){
        printf("i = %d, %d >> (%d - %d - 1)%%2 = %d\n", i, number, size, i, (number >> (size - i - 1)) % 2);
        combinations[i] = (number >> (size - i - 1)) % 2;
    }
    do{
        for(int i = 0; i < size; ++i){
            printf("%d", combinations[i]);
        }
        printf("\n");
        find_next_combination_offset(combinations, size, offset);
    }while(combinations[0] != -1);
    free(combinations);
}

int calculate_offset(int number){
    int result = 0;
    do{
        ++result;
        number /= 2;
    }while(number != 0);
    return result;
}

int* find_min_cover_parallel(struct Graph graph, int thread_number, int max_thread_number){
    int* combinations = (int*)malloc(sizeof(int) * graph.vertex_count), *solution = (int*)malloc(sizeof(int) * graph.vertex_count);
    int offset = calculate_offset(max_thread_number);
    // printf("Offset is %d\n", offset);
    for(int i = 0; i < graph.vertex_count; ++i){
        // printf("i = %d, %d >> (%d - %d - 1)%%2 = %d\n", i, thread_number, graph.vertex_count, i, (thread_number >> (graph.vertex_count - i - 1)) % 2);
        combinations[i] = (thread_number >> (graph.vertex_count - i - 1)) % 2;
    }
    int flag, one_count, current_min = INF;

    do{
        // for(int i = 0; i < graph.vertex_count; ++i){
        //     printf("%d", combinations[i]);
        // }
        // printf("\n");
        find_next_combination_offset(combinations, graph.vertex_count, offset);
        if(combinations[0] == -1) break;
        one_count = 0;
        for(int i = 0; i < graph.vertex_count; ++i){
            if(combinations[i] == 1) ++one_count;
        }
        if(one_count >= current_min) continue;
        // for(int i = 0; i < graph.vertex_count; ++i){
        //     printf("%d", combinations[i]);
        // }
        int tmp_matrix[graph.vertex_count][graph.vertex_count];
        for(int i = 0; i < graph.vertex_count; ++i){
            for(int j = 0; j < graph.vertex_count; ++j){
                if(combinations[i] == 1 || combinations[j] == 1) {
                    tmp_matrix[i][j] = 0;
                }
                else tmp_matrix[i][j] = graph.matrix[i][j];
            }
        }
        flag = 1;
        // printf("\n---\n");
        // for(int i = 0; i < graph.vertex_count; ++i){
        //     for(int j = 0; j < graph.vertex_count; ++j){
        //         printf("%d", tmp_matrix[i][j]);
        //     }
        //     printf("\n");
        // }
        // printf("\n---\n");
        for(int i = 0; i < graph.vertex_count; ++i){
            for(int j = 0; j < graph.vertex_count; ++j){
                if(tmp_matrix[i][j] == 1){
                    flag = 0;
                    break;
                }
            }
            if(!flag) break;
        }
        if(flag){
            if(one_count < current_min){
                // printf("Mamy nowe pokrycie minimalne! Ma %d wierzcholkow.\n", one_count);
                int index = 0;
                for(int i = 0; i < graph.vertex_count; ++i){
                    solution[i] = -1;
                }
                for(int i = 0; i < graph.vertex_count; ++i){
                    if(combinations[i] == 1){
                        solution[index++] = i;
                    }
                }
                current_min = one_count;
            }
        }
    }while(combinations[0] != -1);
    free(combinations);
    return solution;
}

int* find_min_cover_full_search(struct Graph graph){
    int* combinations = (int *)malloc(sizeof(int) * graph.vertex_count), *solution = (int*)malloc(sizeof(int) * graph.vertex_count);
    for(int i = 0; i < graph.vertex_count; ++i){
        combinations[i] = 0;
        solution[i] = -1;
    }
    int one_count;
    int current_min = INF, flag;
    // #pragma omp parallel
    {
    do{
        find_next_combination(combinations, graph.vertex_count);
        if(combinations[0] == -1) break;
        one_count = 0;
        for(int i = 0; i < graph.vertex_count; ++i){
            if(combinations[i] == 1) ++one_count;
        }
        if(one_count >= current_min) continue;
        // for(int i = 0; i < graph.vertex_count; ++i){
        //     printf("%d", combinations[i]);
        // }
        int tmp_matrix[graph.vertex_count][graph.vertex_count];
        for(int i = 0; i < graph.vertex_count; ++i){
            for(int j = 0; j < graph.vertex_count; ++j){
                if(combinations[i] == 1 || combinations[j] == 1) {
                    tmp_matrix[i][j] = 0;
                }
                else tmp_matrix[i][j] = graph.matrix[i][j];
            }
        }
        flag = 1;
        for(int i = 0; i < graph.vertex_count; ++i){
            for(int j = 0; j < graph.vertex_count; ++j){
                if(tmp_matrix[i][j] == 1){
                    flag = 0;
                    break;
                }
            }
            if(!flag) break;
        }
        if(flag){
            if(one_count < current_min){
                // printf("Mamy nowe pokrycie minimalne! Ma %d wierzcholkow.\n", one_count);
                int index = 0;
                for(int i = 0; i < graph.vertex_count; ++i){
                    solution[i] = -1;
                }
                for(int i = 0; i < graph.vertex_count; ++i){
                    if(combinations[i] == 1){
                        solution[index++] = i;
                    }
                }
                current_min = one_count;
            }
        }
    }while(combinations[0] != -1);
    }

    free(combinations);
    return solution;
}

int* find_min_cover_full_search_experimental(struct Graph graph){
    int* combinations = (int *)malloc(sizeof(int) * graph.vertex_count), *solution = (int*)malloc(sizeof(int) * graph.vertex_count);
    for(int i = 0; i < graph.vertex_count; ++i){
        combinations[i] = 0;
        solution[i] = -1;
    }
    int one_count;
    int current_min = INF, flag;
    // #pragma omp parallel
    {
    for(int i = 0; i < graph.vertex_count; ++i){
        find_next_combination(combinations, graph.vertex_count);
        if(combinations[0] == -1) break;
        one_count = 0;
        for(int i = 0; i < graph.vertex_count; ++i){
            if(combinations[i] == 1) ++one_count;
        }
        if(one_count >= current_min) continue;
        // for(int i = 0; i < graph.vertex_count; ++i){
        //     printf("%d", combinations[i]);
        // }
        int tmp_matrix[graph.vertex_count][graph.vertex_count];
        for(int i = 0; i < graph.vertex_count; ++i){
            for(int j = 0; j < graph.vertex_count; ++j){
                if(combinations[i] == 1 || combinations[j] == 1) {
                    tmp_matrix[i][j] = 0;
                }
                else tmp_matrix[i][j] = graph.matrix[i][j];
            }
        }
        flag = 1;
        // printf("\n---\n");
        // for(int i = 0; i < graph.vertex_count; ++i){
        //     for(int j = 0; j < graph.vertex_count; ++j){
        //         printf("%d", tmp_matrix[i][j]);
        //     }
        //     printf("\n");
        // }
        // printf("\n---\n");
        for(int i = 0; i < graph.vertex_count; ++i){
            for(int j = 0; j < graph.vertex_count; ++j){
                if(tmp_matrix[i][j] == 1){
                    flag = 0;
                    break;
                }
            }
            if(!flag) break;
        }
        if(flag){
            if(one_count < current_min){
                // printf("Mamy nowe pokrycie minimalne! Ma %d wierzcholkow.\n", one_count);
                int index = 0;
                for(int i = 0; i < graph.vertex_count; ++i){
                    solution[i] = -1;
                }
                for(int i = 0; i < graph.vertex_count; ++i){
                    if(combinations[i] == 1){
                        solution[index++] = i;
                    }
                }
                current_min = one_count;
            }
        }
    }while(combinations[0] != -1);
    }

    free(combinations);
    return solution;
}

void find_next_combination_experimental(int* combination_array, int size, int length){
    // #pragma omp parallel for
    for(int j = size - 1; j >= -1; --j){
        if(j < 0){
            combination_array[0] = -1;
            break;
        }
        if(combination_array[j] == 0){
            combination_array[j] = 1;
            break;
        }
        else combination_array[j] = 0;
    }
    return;
}


int is_feasible_solution(struct Graph graph, int* solution){
    int obj = 0;
    struct Graph tmp = initialize_graph(graph.vertex_count);
    for(int i = 0; i < tmp.vertex_count; ++i){
        for(int j = 0; j < tmp.vertex_count; ++j){
            tmp.matrix[i][j] = graph.matrix[i][j];
        }
    }

    for(int i = 0; i < tmp.vertex_count; ++i){
        if(solution[i] == 1){
            ++obj;
            for(int j = 0; j < tmp.vertex_count; ++j){
                tmp.matrix[i][j] = 0;
                tmp.matrix[j][i] = 0;
            }
        }
    }
    for(int i = 0; i < tmp.vertex_count; ++i){
        for(int j = 0; j < tmp.vertex_count; ++j){
            if(tmp.matrix[i][j] == 1){
                for(int i = 0; i < tmp.vertex_count; ++i){
                    free(tmp.matrix[i]);
                }
                free(tmp.matrix);
                return 0;
            }
        }
    }
    for(int i = 0; i < tmp.vertex_count; ++i){
        free(tmp.matrix[i]);
    }
    free(tmp.matrix);
    return 1;
}

int objective(int* solution, int length){
    int obj = 0;
    for(int i = 0; i < length; ++i){
        if(solution[i] == 1) ++obj;
    }
    return obj;
}

double worse_solution_probability(double temperature, int* current, int* previous, int size){
    return exp(-1 * (objective(current, 0) - objective(previous, 0)) / temperature);
}

int* find_cover_annealing(struct Graph graph, double START_TEMP, double MIN_TEMP, double COOLING_RATE, int MAX_NEIGHBOURS){
    srand(time(NULL));
    if(graph.vertex_count <= 1){
        int* solution = (int*)malloc(sizeof(int));
        solution[0] = -1;
        return solution;
    }
    int* initial_best_solution = find_cover_heuristic(graph), *current_solution = (int*)malloc(sizeof(int) * graph.vertex_count);
    int* best_solution = (int*)malloc(sizeof(int) * graph.vertex_count);
    for(int i = 0; i < graph.vertex_count; ++i){
        best_solution[i] = 0;
    }
    for(int i = 0; i < graph.vertex_count; ++i){
        if(initial_best_solution[i] == -1){
            break;
        }
        best_solution[initial_best_solution[i]] = 1;
    }
    int changes, index, index2, tmp;
    // comment this out if you want better results
    // this is only executed to better show how parallelly executing many instances
    // of this function increases the quality of the found solution
    // (due to this function being random)
    for(int i = 0; i < graph.vertex_count; ++i){
        best_solution[i] = 1;
    }
    double temperature = START_TEMP;

    while(temperature > MIN_TEMP){
        for(int i = 0; i < graph.vertex_count; ++i){
            current_solution[i] = best_solution[i];
        }
        changes = rand() % 2 + 1;
        if(changes == 1){
            index = rand() % graph.vertex_count;
            if(current_solution[index] == 0){
                current_solution[index] = 1;
            }
            else{
                current_solution[index] = 0;
            }
        }
        else{
            index = rand() % graph.vertex_count;
            index2 = rand() % graph.vertex_count;
            while(index == index2){
                index = rand() % graph.vertex_count;
                index2 = rand() % graph.vertex_count;
            }
            tmp = current_solution[index];
            current_solution[index] = current_solution[index2];
            current_solution[index2] = tmp;
        }
        if(is_feasible_solution(graph, current_solution)){
            if(objective(current_solution, graph.vertex_count) <= objective(best_solution, graph.vertex_count) || rand() < worse_solution_probability(temperature, current_solution, best_solution, graph.vertex_count)){
                for(int i = 0; i < graph.vertex_count; ++i){
                    best_solution[i] = current_solution[i];
                }
            }
        }
        temperature *= COOLING_RATE;
    }

    int* true_best_solution = (int*)malloc(sizeof(int) * graph.vertex_count);
    int current_index = 0;
    for(int i = 0; i < graph.vertex_count; ++i){
        true_best_solution[i] = -1;
        if(best_solution[i] == 1){
            true_best_solution[current_index++] = i;
        }
    }
    free(initial_best_solution);
    free(best_solution);
    free(current_solution);
    return true_best_solution;
}

void generate_and_solve(int graphs, int vertices, int probability){
    struct Graph* testing_edge_list = generate_random_graphs(graphs, vertices, probability);
    struct Graph summed_graph = sum_graphs(testing_edge_list, graphs);
    int* heur_solution = find_cover_heuristic(summed_graph);

    free(testing_edge_list);
    free(heur_solution);
}

void solve_sequentially(int data_sets, int testing_graphs, int testing_vertices, int testing_probability){
    struct Graph* testing_edge_list;
    struct Graph summed_graph;
    int* heur_solution;
    for(int i = 0; i < data_sets; ++i){
        // printf("%d\n", i);
        testing_edge_list = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
        summed_graph = sum_graphs(testing_edge_list, testing_graphs);
        heur_solution = find_cover_heuristic(summed_graph);
        // for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
        //     if(heur_solution[j] == -1){
        //         heur_solution = (int*)realloc(heur_solution, sizeof(int) * (j));
        //         heur_cover_vertex_count = j;
        //         break;
        //     }
        // }
    }
}

void solve_parallelly(int data_sets, int testing_graphs, int testing_vertices, int testing_probability, int testing_threads){
    struct Graph* testing_edge_list;
    struct Graph summed_graph;
    int* heur_solution;
    #pragma omp parallel for num_threads(testing_threads) private(testing_edge_list, summed_graph, heur_solution)
    for(int i = 0; i < data_sets; ++i){
        // printf("%d\n", i);
        testing_edge_list = generate_random_graphs(testing_graphs, testing_vertices, testing_probability);
        summed_graph = sum_graphs(testing_edge_list, testing_graphs);
        heur_solution = find_cover_heuristic(summed_graph);
        // for(int j = 0; j < testing_edge_list[0].vertex_count; ++j){
        //     if(heur_solution[j] == -1){
        //         heur_solution = (int*)realloc(heur_solution, sizeof(int) * (j));
        //         heur_cover_vertex_count = j;
        //         break;
        //     }
        // }
    }
}